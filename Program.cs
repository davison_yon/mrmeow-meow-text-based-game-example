// You are to create a simple text game
// You will create a simple story line with
// Branching options, given user input

Console.WriteLine("The Adventures of Mr. Meow Meow");
Console.WriteLine("//////////////////");

Console.WriteLine("Once upon a time once " +
    "lived a cat named Mr. Meow Meow. He " +
    "was a hero to his people. He had once " +
    "defeated the great dragon of the North and " +
    "de-throwned the evil King. Now, Mr. Meow " +
    "Meow needs your help. A new threat threatens" +
    "the town. How will you raise to the " +
    "ocassion? ");

Console.WriteLine("You begin to cross a dirt " +
    "road and notice there is a piece of paper " +
    "on the road. What do you do? 1) Read the " +
    "paper or 2) Continue walking");
string choiceOne = Console.ReadLine();

if (choiceOne == "1")
{
    Console.WriteLine("You read the paper!");
    ReadPaper();
}
if (choiceOne == "2")
{
    Console.WriteLine("You continue walking!");
    ContinueWalking();
}


void ReadPaper()
{
    Console.Clear(); //optional 
    //Continues story
    Console.WriteLine("You pick up the paper off" +
        "of the ground and see that it is " +
        "actually a newspaper. Written in black " +
        "ink at the top is a message that reads " +
        "\" Help Wanted! Seak Mr. M M! \". What " +
        "do you do? 1) Head to the city to search " +
        "for Mr. M or 2) Continue down the road");
    string choiceTwo = Console.ReadLine();
    if (choiceTwo == "1")
    {
        HeadToCity();
    }
    if (choiceTwo == "2")
    {
        GameOver();
    }
}

void ContinueWalking()
{
    Console.Clear();
    //Continues story
    Console.WriteLine("You ignore the paper and continue " +
        "walking. All of the sudden, a strange figure " +
        "appears in the horizon. What do you do? 1) " +
        "Approach the Figure or 2) Ignore the Figure. ");

    string choiceTwo = Console.ReadLine();

    if (choiceTwo == "1")
    {
        TalkToFigure();
    }

    if (choiceTwo =="2")
    {
        GameOver();
    }
}

void HeadToCity()
{
    Console.Clear();
    Console.WriteLine("You head to the City and meet " +
        "with Mr. Meow Meow. You save the world!");
}

void TalkToFigure()
{
    Console.Clear();
    Console.WriteLine("You approach the figure to " +
        "discover it is Mr. Meow Meow. He recruits you " +
        "to his cause and you save the World.");
}

void GameOver()
{
    Console.Clear();
    Console.WriteLine("You ignored Mr. Meow Meow. " +
        "The world has exploded. RIP. Game over.");
}
